import express from "express";
import graphqlHTTP from "express-graphql";
import { resolvers } from "./resolvers";
import schema from "./schema";
const app = express();

app.get('/', (req, res) => {
    res.json({
        message: 'hola mundo'
    })
})

app.use('/graphql', graphqlHTTP({
    graphq: true,
    schema: schema
}));
app.listen(4000, () => console.log('Server on port 4000'));