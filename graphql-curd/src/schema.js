import { makeExecutableSchema} from "graphql-tools";
import { resolvers } from "./resolvers";
const typeDefs = '
    type Query {
     hello: String
     greet( name: String! ): String,
     greet( id : String! ): String,
     greet( email : String! ): String,
     greet( age: String! ): String,
     greet( address : String! ): String,
     greet( phone : String! ): String
} 
} 

';

export default makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers
})