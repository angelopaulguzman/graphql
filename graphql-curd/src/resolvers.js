export const resolvers = {
    Query: {
    hello: () => {
        return 'Hello World with GraphQL'
    },
    greet(root, {name, id, email, age, address, phone}){
        return 'Hello! ${name} ${id} ${email} ${age} ${address} ${phone}';
    }
}
}